export default {
  methods: {


    addItem: function () {
      this.invoice.items.push(
        {
          name: this.new_item.name,
          description: this.new_item.description,
          price: this.new_item.price,
          quantity: this.new_item.quantity
        }
      );
      this.new_item.name = '';
      this.new_item.description = '';
      this.new_item.price = '';
      this.new_item.quantity = '';
    },


    addOtherCost: function () {
      this.invoice.other_costs.push(
        {
          description: this.new_other_cost.description,
          price_or_percent: this.new_other_cost.price_or_percent,
          type: this.new_other_cost.type
        }
      );
      this.new_other_cost.description = '';
      this.new_other_cost.price_or_percent = '';
      this.new_other_cost.type = '';
    },


    delItem: function (chosenList, item) {
      chosenList.splice(chosenList.indexOf(item), 1)
    },


    delLogo: function () {
      this.invoice.company_logo = null;
    },


    setBase64: function () {
      let file = document.getElementById("company-logo").files[0];
      let reader = new FileReader();
      var invoice = this.invoice;
      reader.readAsDataURL(file);
      reader.onload = function () {
        invoice.company_logo = reader.result;
      };
      reader.onerror = function (error) {
        console.log(error)
      }
    }


  }
}
