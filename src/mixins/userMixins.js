import axios from 'axios';

export default {
  methods: {
    loginUser: function (email, password) {
      const payload = {
        email: email,
        password: password
      };
      axios.post(this.$store.state.endpoints.obtainJWT, payload)
        .then((response) => {
          console.log(response.data);
          this.$store.commit('updateToken', response.data.access_token);
          const base = {
            baseURL: this.$store.state.endpoints.baseUrl,
            headers: {
              Authorization: `Bearer ${this.$store.state.jwt}`,
              'Content-Type': 'application/json'
            },
            xhrFields: {
              withCredentials: true
            }
          };
          const axiosInstance = axios.create(base);
          axiosInstance({
            url: "/user/login",
            method: "get",
            params: {}
          })
            .then((response) => {
              this.$store.commit('setAuthUser', {authUser: response.data.username, isAuthenticated: true});
              this.$router.push({name: 'Home'})
            })
        })
        .catch((error) => {
          console.log(error)
        })
    }
  }
}
