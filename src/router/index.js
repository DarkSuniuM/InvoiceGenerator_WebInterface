import Home from '../components/Home'
import Register from '../components/Register'
import Login from '../components/Login'
import Account from '../components/Account'
import InvoiceList from '../components/InvoiceList'
import CreateInvoice from '../components/CreateInvoice'
import EditInvoice from '../components/EditInvoice'


export default [
  {path: '/', name: 'Home', component: Home},
  {path: '/register', name: 'Register', component: Register},
  {path: '/login', name: 'Login', component: Login},
  {path: '/account', name: 'Account', component: Account, meta: {requiresAuth: true}},
  {path: '/invoices', name: 'InvoiceList', component: InvoiceList, meta: {requiresAuth: true}},
  {path: '/invoices/new', name: 'CreateInvoice', component: CreateInvoice, meta: {requiresAuth: true}},
  {path: '/invoices/edit/:id', name: 'EditInvoice', component:EditInvoice, meta: {requiresAuth: true}}
]
