import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import normalize from 'normalize.css'
import Routes from './router'
import store from './vuex/store'

Vue.use(VueRouter);

const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.state.isAuthenticated) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
});

new Vue({
  el: '#app',
  render: h => h(App),
  router: router,
  store: store
});




